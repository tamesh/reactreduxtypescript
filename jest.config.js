const { defaults: tsjPreset } = require('ts-jest/presets');

module.exports = {
    ...tsjPreset,
    preset: 'react-native',
    setupFiles: ['./src/setupJest.js'],
    transform: {
        ...tsjPreset.transform,
    },
    transformIgnorePatterns: [
    ],
    testRegex: '/__tests__/.+\\.test\\.tsx?$',
    moduleFileExtensions: [
        'ts',
        'tsx',
        'js',
        'jsx',
        'json',
        'node',
    ],
    collectCoverageFrom: [
        '**/*.tsx',
        '**/*.ts',
        '!src/Store.ts',
        '!src/Config.ts',
        '!src/images/index.ts',
        '!src/@types/**/*.ts',
        '!scripts/**/*.ts',
    ],
    coverageReporters: [
        'text',
        'lcov',
    ],
    moduleNameMapper: {
        '^src(.*)': '<rootDir>/src/$1',
    },
    globals: {
        'ts-jest': {
            babelConfig: true,
            tsConfig: 'tsconfig.test.json',
        },
    },
    cacheDirectory: '.jest/cache',
};
