import './App.css';
import React from 'react';
import { connect } from "react-redux";
import { Dispatch } from "redux";
import { AppState } from "./store";
import { User } from "./store/user/types";
import { editAction, deleteAction, saveRedord } from "./store/user/action";

import { ListComponent } from './components/list-component';
import { UserFormContainer } from './components/user-form';

interface AppProps {
  user: User;
  userList: User[];
  editRecord: Function;
  deleteRecord: Function;
  handleSubmit: any;
}


export class App extends React.Component<AppProps, AppState> {
  
  componentDidMount() {}

  render() {
    const { user, userList, handleSubmit, editRecord, deleteRecord } = this.props;
    return (
      <div className="app-cls">
        <div>
          <UserFormContainer onSubmit={handleSubmit} initialValues={user}/>
        </div>
        <div>
          <ListComponent dataList={userList} editAction={editRecord} deleteAction={deleteRecord} />
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state: AppState) => ({
  user: state.users.user,
  userList: state.users.userList
});

const mapDispatchToProps = (dispatch: Dispatch) => ({
  editRecord: (data: User) => dispatch(editAction(data)),
  deleteRecord: (id: number) => dispatch(deleteAction(id)),
  handleSubmit: (values: User) => dispatch(saveRedord(values))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App);
