import React from 'react';
import { User } from '../store/user/types';

interface ListInterface { 
    dataList: Array<User>;
    editAction: Function;
    deleteAction: Function;
};

export const ListComponent = (props: ListInterface) => {
    const { dataList, editAction, deleteAction } = props;
    return (
        <div>
            <table className="table table-bordered table-striped">
                <thead className="thead-light">
                    <tr>
                        <th scope="col" className="">#</th>
                        <th scope="col" className="">Name</th>
                        <th scope="col" className="">Role</th>
                        <th scope="col" className="">Departments</th>
                        <th scope="col" className="">Authorised</th>
                        <th scope="col" className="">Notes</th>
                        <th scope="col" className="">Action</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        dataList && dataList.map((item, ind) => 
                    <tr key={item.id}>
                        <th scope="row">{ind+1}</th>
                        <td>
                            <div>{item.userName}</div>
                            <sub>{item.userEmail} </sub>
                            <sub>{item.userPhone} </sub>
                            <sub>{item.userAddress} </sub>
                        </td>
                        <td>{item.userRole}</td>
                        <td>
                            <ul className="list-unstyled">
                                { 
                                    item.userDepartment.map((dp,i) => <li key={dp.val}>{ dp.label }</li>) 
                                }
                            </ul>
                        </td>
                        <td>
                            <em className={item.userIsAdmin ? 'fa fa-unlock text-success' : 'fa fa-lock text-danger'}></em>
                        </td>
                        <td>{item.userNotes}</td>
                        <td>
                            <em className="fa fa-pencil-square mr-3 text-success" onClick={() => editAction(item)}></em>
                            <em className="fa fa-trash text-danger" onClick={() => deleteAction(item.id)}></em>
                        </td>
                    </tr>
                    
                    )}
                </tbody>
            </table>
        </div>
    )
}
