import * as React from "react";
import { reduxForm, InjectedFormProps, Field, reset } from "redux-form";

import { User, UserState } from "../store/user/types";
import { connect } from 'react-redux';

import { renderDropdownList, renderMultiselect } from "./customElements";

interface InjectedProps extends InjectedFormProps<User, {}> { }
interface ComponentProps extends User, InjectedProps { 
    onClearChanges: any
};
interface ComponentState { };

class UserForm extends React.Component<ComponentProps, ComponentState> {
    render() {
        const { pristine, valid, submitting, handleSubmit, initialValues, onClearChanges } = this.props;
        const renderButton = () => {
            let button;

            if (initialValues && initialValues.id) {
                button = <button type="button" className="btn btn-success mr-4" onClick={handleSubmit} disabled={!valid || pristine || submitting}>Update</button>;
            } else {
                button = <button type="submit" className="btn btn-success mr-4" onClick={handleSubmit} disabled={!valid || pristine || submitting}>Submit</button>;
            }
            return button;
        };
        return (
            <div className="p-4">
            <form onSubmit={handleSubmit}>
                <div className="form-row">
                    <div className="form-group col-md-6">
                        <div className="">
                            <label>First Name </label>
                            <Field
                                name="userName"
                                component="input"
                                type="text"
                                className="form-control"
                                autoFocus
                            />
                        </div>
                        <div className="">
                            <label>Email</label>
                            <Field
                                name="userEmail"
                                component="input"
                                type="text"
                                className="form-control"
                            />
                        </div>
                        <div className="">
                            <label>Phone</label>
                            <Field
                                name="userPhone"
                                component="input"
                                type="text"
                                className="form-control"
                            />
                        </div>
                        <div className="">
                            <label>Address</label>
                            <Field
                                name="userAddress"
                                component="input"
                                type="text"
                                className="form-control"
                            />
                        </div>
                    </div>
                    <div className="form-group col-md-6">
                        <div className="">
                            <label>Role</label>
                            <Field
                                name="userRole"
                                component={renderDropdownList}
                                type="text"
                                className="form-control"
                                data={['Admin', 'User','Developer']}
                            />
                        </div>
                        <div className="">
                            <label>Departments</label>
                            <Field
                                name="userDepartment"
                                component={renderMultiselect}
                                type="text"
                                data={[{ label: 'Admin', val: 'AD' }, 
                                    { label: 'Finance', val: 'FN' }, { label: 'Information Tech', val: 'IT' }]}
                                className="form-control"
                                valueField="val"
                                textField="label"
                            />
                        </div>
                        <div className="">
                            <label>Notes</label>
                            <Field
                                name="userNotes"
                                component="input"
                                type="text"
                                className="form-control"
                            />
                        </div>
                        <div className="">
                            <label>Authorised</label>
                            <Field
                                name="userIsAdmin"
                                component="input"
                                type="checkbox"
                                className="form-control"
                            />
                        </div>
                    </div>
                </div>
                
                <div className="mb-4">
                    {renderButton()}
                    <button type="button" className="btn btn-primary" onClick={onClearChanges}>Clear Values</button>
                </div>
            </form>
            </div>
        );
    }
}

const resetForm = () => {
    return {
        type: 'RESET_USER',
    };
};

const mapStateToProps = (state: UserState, ownProps: InjectedProps) => ({
    user: state.user,
});

const mapDispatchToProps = (dispatch) => ({
    onClearChanges: () => {
        dispatch(resetForm());
        dispatch(reset('userForm'));
    },
});

const ConnectedReduxForm = connect(mapStateToProps, mapDispatchToProps)(UserForm);

const UserReduxForm = reduxForm<User>({
    form: 'userForm',
    onSubmitSuccess: (result, dispatch) => {
        dispatch(resetForm());
        dispatch(reset('userForm'));
    },
    enableReinitialize: true,
})(ConnectedReduxForm);

export const UserFormContainer = UserReduxForm;