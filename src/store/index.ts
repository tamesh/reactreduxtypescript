import { createStore, combineReducers, applyMiddleware } from "redux";
import thunkMiddleware from "redux-thunk";
import { composeWithDevTools } from "redux-devtools-extension";
import { reducer as reduxFormReducer } from 'redux-form';

import { userReducers } from "./user/reducers";

const rootReducer = combineReducers({
    users: userReducers,
    form: reduxFormReducer
});

export type AppState = ReturnType<typeof rootReducer>;

const configureStore = () => {
    const middlewares = [thunkMiddleware];
    const middleWareEnhancer = applyMiddleware(...middlewares);

    const store = createStore(
        rootReducer,
        composeWithDevTools(middleWareEnhancer)
    );

    return store;
}
export default configureStore;
export const getDispatch = () => configureStore().dispatch;
