import { 
    // UserState, 
    User, 
    EDIT_USER, 
    DELETE_USER ,
    SAVE_USER,
    UPDATE_USER,
    RESET_USER
} 
from "./types";

export const uuidv4 = () => {
    return (`${[1e7]}${1e3}${4e3}${8e3}${1e11}`).replace(/[018]/g, (c: any) =>
        // eslint-disable-next-line no-mixed-operators
        (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
    );
}

export const updateObjectInArray = (array, data) => {
    return array.map((item, index) => {
        if (item.id !== data.id) {
            return item;
        }
        return {
            ...item,
            ...data
        }
    })
}

export const editAction = (data: User) => {
    return {
        type: EDIT_USER,
        payload: {user: data}
    };
}

export const deleteAction = (id: number) => {
    return {
        type: DELETE_USER,
        payload: id
    };
}

export const saveRedord = (data: User) => {
    if( data.id){
        return {
            type: UPDATE_USER,
            payload: data
        }   
    } else {
        return {
            type: SAVE_USER,
            payload: { ...data, id: uuidv4() }
        }
    }
}

export const resetUser = () => {
    return {
        type: RESET_USER
    }
}
