import { Action } from 'redux';
import { 
    UserState,
    UPDATE_USER, UserActionTypes,
    EDIT_USER, EditUserAction, 
    DELETE_USER, DeleteUserAction,
    SAVE_USER, SaveUser,
    RESET_USER
 } from "./types";
import { uuidv4, updateObjectInArray } from './action';

const initialState: UserState = {
    user: {
        id: null,
        userName: null,
        userPhone: null,
        userEmail: null,
        userAddress: null,
        userIsAdmin: null,
        userRole: null,
        userDepartment: null,
        userNotes: null
    },
    userList: [{
        id: uuidv4(),
        userName: 'Test Nirmalkar',
        userPhone: 1234567890,
        userEmail: 'test@test.com',
        userAddress: "this is address field",
        userIsAdmin: false,
        userRole: 'User',
        userDepartment: [{label: 'Finance', val: 'FN'}],
        userNotes: 'this is finance user'
    }, {
        id: uuidv4(),
        userName: 'Falaksh Nirmalkar',
        userPhone: 1234567890,
        userEmail: 'f@f.com',
        userAddress: "this is address field",
        userIsAdmin: true,
        userRole: 'Admin',
        userDepartment: [{ label: 'Admin', val: 'AD' }],
        userNotes: 'this is admin role'
    },
    {
        id: uuidv4(),
        userName: 'Shilpi Nirmalkar',
        userPhone: 1234567890,
        userEmail: 'm@m.com',
        userAddress: "this is address field",
        userIsAdmin: false,
        userRole: 'Teacher',
        userDepartment: [{ label: 'School', val: 'SC' }],
        userNotes: 'this is teacher role'
    }],
};

export const userReducers = (
    state = initialState,
    action: Action
): UserState => {
    switch (action.type) {
        case SAVE_USER: {
            const { payload } = action as SaveUser;
            return {
                ...state,
                userList: state.userList.concat(payload)
            };
        }
        case UPDATE_USER: {
            const { payload } = action as UserActionTypes;
            let updatedList = { userList: updateObjectInArray(state.userList, payload) };
            return {
                ...state,
                ...updatedList
            };
        }
        case EDIT_USER: {
            const { payload } = action as EditUserAction;
            return {
                ...state,
                ...payload
            };
        }
        case DELETE_USER: {
            const { payload } = action as DeleteUserAction;
            return {
                ...state,
                userList: state.userList.filter(item => item.id !== payload)
            };
        }

        case RESET_USER: {
            return {
                ...state,
                user: initialState.user
            };
        }
        default:
            return state;
    }
}
