// Describing the shape of the system's slice of state
export interface User {
    id: string;
    userName: string;
    userEmail: string;
    userPhone: number;
    userAddress: string;
    userIsAdmin: boolean;
    userRole: string;
    userDepartment: { label: string, val: string }[];
    userNotes: string;
};

export interface UserState {
    user: User;
    userList: User[];
};

// Describing the different ACTION NAMES available
export const UPDATE_USER = "UPDATE_USER";
export const EDIT_USER = "EDIT_USER";
export const DELETE_USER = "DELETE_USER";
export const SAVE_USER = "SAVE_USER";
export const RESET_USER = "RESET_USER";

interface UpdateUserAction {
    type: typeof UPDATE_USER;
    payload: UserState;
}

export type UserActionTypes = UpdateUserAction;

export interface DeleteUserAction {
    type: typeof DELETE_USER,
    payload: string;
}
export interface EditUserAction {
    type: typeof EDIT_USER,
    payload: User;
}

export interface SaveUser {
    type: typeof SAVE_USER,
    payload: User;
}

export interface ResetUserAction {
    type: typeof RESET_USER,
    payload: User
}
